const Jobs = require('../models/Jobs');

exports.getIndex = (req, res) => {
	 const jobs = new Jobs();
	 jobs.sheet()
	 .then(sheet => {
	 	let rows = sheet.rows;
	 	res.render('index', {rows: rows});
	 })
	 .catch(err => {
	 	console.log(err);
	 });
};

exports.getListing = (req, res) => {

	const id = req.params.id;

	const jobs = new Jobs();
	 jobs.sheet()
	 .then(sheet => {
	 	let rows = sheet.rows;
	 	const row = rows.find(row => {
	 		row.id === id;
	 	});
	 	res.json(row);
	 	//res.render('listing', {rows: rows});
	 })
	 .catch(err => {
	 	console.log(err);
	 });
};

exports.getContact = (req, res) => {
	res.render('postJob');
}