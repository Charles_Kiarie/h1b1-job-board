const express = require('express');
const bodyPasrser = require('body-parser');
const path = require('path');


const app = express();
const PORT = process.env.port || 3000;

const routes = require('./routes/routes');

app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');
app.set('views', 'views');
app.use(bodyPasrser.urlencoded({ extended: false }));


app.use(routes);

app.listen(PORT);