const express = require('express');
const router = express.Router();

const indexController = require('../controllers/index');

router.get('/', indexController.getIndex);
router.get('/:title/:id', indexController.getListing);
router.get('/contact', indexController.getContact);


module.exports = router;