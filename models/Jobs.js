const sheetsy = require('sheetsy');
const {urlToKey, getWorkbook, getSheet} = sheetsy;

const key = urlToKey('https://docs.google.com/spreadsheets/d/1iKbsR1Du_V1zioHKkUnBLPFn--WYjwxDglgUC0f4S8Y/edit?usp=sharing');

class Jobs {
	constructor(id) {
		this.id = id;
	}

	workbook() {
		return getWorkbook(key)
		.then(workbook => {
			console.log(workbook);
			return workbook;
		})
		.catch(err => {
			console.log(err);
		});
	}

	sheet() {
		return getSheet(key, 'od6')
		.then(sheet => {
			//console.log(sheet);
			return sheet;
		})
		.catch(err => {
			console.log(err);
		});
	}
}

module.exports = Jobs;